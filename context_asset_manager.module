<?php

/**
 * @file
 * Module file for context asset manager.
 */

/**
 * Pull the url of the admin.
 *
 * Future proofing the admin url and theming functions.
 *
 * @return string
 *   the path for context add assets admin page
 */
function _context_asset_manager_admin_path() {
  if (module_exists('context_ui')) {
    $core_context_items = context_ui_menu();
    foreach ($core_context_items as $path => $item) {
      if (stripos($path, 'context/settings') != FALSE) {
        $pageassets_admin_path = str_replace('/settings', '', $path) . '/page-assets-settings';
        break;
      }
    }
    if (!isset($pageassets_admin_path) or !trim($pageassets_admin_path)) {
      $pageassets_admin_path = 'admin/structure/context/page-assets-settings';
    }
    return $pageassets_admin_path;
  }
  else {
    return '';
  }
}

/**
 * Implements hook_init().
 *
 * Used to purge session values and attaching admin css/js file.
 */
function context_asset_manager_init() {
  global $conf;
  $conf['context_asset_manager']['context_asset_manager_js_list'] = [];
  $conf['context_asset_manager']['context_asset_manager_css_list'] = [];
  $conf['context_asset_manager']['context_asset_manager_async_js_list'] = [];
  if (path_is_admin(current_path())) {
    drupal_add_css(drupal_get_path('module', 'context_asset_manager') . '/context_asset_manager.css');
    drupal_add_js(drupal_get_path('module', 'context_asset_manager') . '/context_asset_manager.js');
  }
}

/**
 * Implements hook_js_alter().
 *
 * Used to purge and inject required js files.
 */
function context_asset_manager_js_alter(&$javascript) {
  global $conf;
  $clean_js = variable_get('context_asset_manager_purge_js');
  $clean_all_js = variable_get('context_asset_manager_purge_js_full');
  if ($clean_js || $clean_all_js) {
    if (isset($conf['context_asset_manager']['context_asset_manager_js_list']) && count($conf['context_asset_manager']['context_asset_manager_js_list']) > 0) {
      foreach ($javascript as $name => $jssetings) {
        if (!in_array($name, $conf['context_asset_manager']['context_asset_manager_js_list']) && strpos($name, 'jquery_update/') === FALSE) {
          if ($clean_all_js || ($clean_js && strpos($name, 'sites/') !== FALSE)) {
            unset($javascript[$name]);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_css_alter().
 *
 * Used to purge and inject required css files.
 */
function context_asset_manager_css_alter(&$css) {
  global $conf;
  $clean_css = variable_get('context_asset_manager_purge_css');
  $clean_all_css = variable_get('context_asset_manager_purge_css_full');
  if ($clean_css || $clean_all_css) {
    if (isset($conf['context_asset_manager']['context_asset_manager_css_list']) && count($conf['context_asset_manager']['context_asset_manager_css_list']) > 0) {
      foreach ($css as $name => $csssetings) {
        if (!in_array($name, $conf['context_asset_manager']['context_asset_manager_css_list'])) {
          if ($clean_all_css || ($clean_css && strpos($name, 'sites/') !== FALSE)) {
            unset($css[$name]);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_preprocess_html_tag().
 *
 * Used to attach asycn property for js files.
 */
function context_asset_manager_preprocess_html_tag(&$vars) {
  global $conf;
  $async_js_list = $conf['context_asset_manager']['context_asset_manager_async_js_list'];
  if ($vars['element']['#tag'] == 'script') {
    $script_src = parse_url($vars['element']['#attributes']['src']);
    $script_src_relative = trim($script_src['path'], '/');
    if (in_array($script_src_relative, $async_js_list)) {
      $vars['element']['#attributes']['async'] = 'true';
    }
  }
}

/**
 * Implements hook_menu().
 */
function context_asset_manager_menu() {
  $items[_context_asset_manager_admin_path()] = array(
    'title' => 'Page Assets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('context_asset_manager_admin'),
    'access arguments' => array('access context asset manager administration'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function context_asset_manager_permission() {
  return array(
    'access context asset manager administration' => array(
      'title' => t('Access Context Asset Manager Administration'),
    ),
  );
}

/**
 * Menu Callback.
 *
 * Builds the admin settings page.
 */
function context_asset_manager_admin() {

  $form['context_asset_manager_alert'] = array(
    '#type' => 'item',
    '#markup' => '<div class="context-asset-manager-admin-form-warning">This page needs jQuery version >= 1.6 to work properly.</div>',
  );

  $form['context_asset_manager_include_core_assets'] = array(
    '#type' => 'checkbox',
    '#title' => 'List Drupal Core module/theme assets',
    '#description' => 'Allows you to scan and list assets outside of "sites" folder while selecting for pages',
    '#default_value' => variable_get('context_asset_manager_include_core_assets', 0),
  );

  $modules = module_list();
  asort($modules);
  $contrib_modules = [];
  $core_modules = [];
  foreach ($modules as $key => $value) {
    $index = drupal_get_path('module', $value);

    if (strpos($index, 'sites/') !== FALSE) {
      // We are keeping jquery_update module out for safety.
      if (strpos($index, 'jquery_update') == FALSE) {
        $contrib_modules[$index] = $value;
      }
    }
    else {
      $core_modules[$index] = $value;
    }
    unset($modules[$key], $index);
  }
  if (count($contrib_modules) > 1) {
    $contrib_modules = array_merge(['contrib_select_all' => 'Select/De-select All'], $contrib_modules);
  }

  $core_modules = array_merge(['core_select_all' => 'Select/De-select All'], $core_modules);

  $form['modules'] = array(
    '#type' => 'fieldset',
    '#title' => 'Scan Active Modules',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['modules']['context_asset_manager_scan_contrib_modules'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Select contrib/custom modules to be scanned for resources ( css/js )',
    '#options' => $contrib_modules,
    '#default_value' => variable_get('context_asset_manager_scan_contrib_modules', array()),
  );
  $form['modules']['context_asset_manager_scan_core_modules'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Select core modules to be scanned for resources ( css/js )',
    '#options' => $core_modules,
    '#default_value' => variable_get('context_asset_manager_scan_core_modules', array()),
  );

  $form['description'] = array(
    '#markup' => t('By default Page Resource Manager scans your active themes to list all available css/js files. But you can add custom folders to be scanned below.'),
  );
  $form['paths'] = array(
    '#type' => 'fieldset',
    '#title' => 'Scan Additional folders',
    '#description' => t('Add file paths below to have them scan for other resources. Note that paths should be relative to root of Drupal installation and <em>not include a leading/trailing slash</em>.<p><strong>Example:</strong> sites/all/libraries/my-custom-libraries</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Initialize path array index at zero.
  $path_index = 0;

  // Build form elements for existing path_index vars.
  while ($path = variable_get('context_asset_manager_index_path' . $path_index, NULL)) {
    $form['paths']['context_asset_manager_index_path' . $path_index] = array(
      '#type'   => 'textfield',
      '#title'  => 'Path',
      '#default_value' => $path,
    );
    $path_index += 1;
  }

  // Set how many empty forms to show.
  $path_add_at_time = 1;

  // Set loop end (based on path_add_at_time)
  // -1 for 0 indexed array.
  $end = $path_index + $path_add_at_time - 1;

  while ($path_index <= $end) {
    $form['paths']['context_asset_manager_index_path' . $path_index] = array(
      '#type'   => 'textfield',
      '#title'  => 'Path',
    );
    $path_index += 1;
  }

  $form['purge_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Resource Purge Settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['purge_settings']['context_asset_manager_purge_css'] = array(
    '#type' => 'checkbox',
    '#title' => 'Purge existing theme/module css ( Excludes drupal core css )',
    '#description' => 'Remove existing css files coming from themes/modules before page rendering. Helps you to clean before you cherry pick your own css files.',
    '#default_value' => variable_get('context_asset_manager_purge_css', 0),
  );

  $form['purge_settings']['context_asset_manager_purge_js'] = array(
    '#type' => 'checkbox',
    '#title' => 'Purge existing theme/module js ( Excludes drupal core js )',
    '#description' => 'Remove existing js files coming from themes/modules before page rendering. Helps you to clean before you cherry pick your own js files.',
    '#default_value' => variable_get('context_asset_manager_purge_js', 0),
  );
  $form['purge_settings']['context_asset_manager_purge_css_full'] = array(
    '#type' => 'checkbox',
    '#title' => 'Purge all existing css ( Includes drupal core css )',
    '#description' => 'Remove all existing css files before page rendering. <strong>Should be used by advanced user as it removes drupal core css files.</strong>',
    '#default_value' => variable_get('context_asset_manager_purge_css_full', 0),
  );

  $form['purge_settings']['context_asset_manager_purge_js_full'] = array(
    '#type' => 'checkbox',
    '#title' => 'Purge all existing js ( Includes drupal core js )',
    '#description' => 'Remove all existing js files before page rendering. <strong>Should be used by advanced user as it removes drupal core js files.</strong>',
    '#default_value' => variable_get('context_asset_manager_purge_js_full', 0),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_ctools_plugin_api().
 */
function context_asset_manager_ctools_plugin_api($module, $api) {
  if ($module == 'context' && $api == 'plugins') {
    return array('version' => 3);
  }
}

/**
 * Implements hook_context_plugins().
 */
function context_asset_manager_context_plugins() {
  $plugins = array();

  // Theme Assets
  // - CSS.
  $plugins['context_asset_manager_addcss'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addcss.inc',
      'class' => 'context_asset_manager_addcss',
      'parent' => 'context_reaction',
    ),
  );
  // - LESS.
  if (module_exists('less')) {
    $plugins['context_asset_manager_addless'] = array(
      'handler' => array(
        'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
        'file' => 'context_asset_manager_addless.inc',
        'class' => 'context_asset_manager_addless',
        'parent' => 'context_reaction',
      ),
    );
  }
  // - JS.
  $plugins['context_asset_manager_addjs'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addjs.inc',
      'class' => 'context_asset_manager_addjs',
      'parent' => 'context_reaction',
    ),
  );

  // Module Assets
  // - CSS.
  $plugins['context_asset_manager_addcss_module'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addcss_module.inc',
      'class' => 'context_asset_manager_addcss_module',
      'parent' => 'context_reaction',
    ),
  );

  // - JS.
  $plugins['context_asset_manager_addjs_module'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addjs_module.inc',
      'class' => 'context_asset_manager_addjs_module',
      'parent' => 'context_reaction',
    ),
  );

  // File Path Assets
  // - CSS.
  $plugins['context_asset_manager_addcss_path'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addcss_path.inc',
      'class' => 'context_asset_manager_addcss_path',
      'parent' => 'context_reaction',
    ),
  );
  // - JS.
  $plugins['context_asset_manager_addjs_path'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_asset_manager') . '/plugins',
      'file' => 'context_asset_manager_addjs_path.inc',
      'class' => 'context_asset_manager_addjs_path',
      'parent' => 'context_reaction',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function context_asset_manager_context_registry() {
  $registry = array();

  $registry['reactions'] = array(
    // Theme.
    'css' => array(
      'plugin' => 'context_asset_manager_addcss',
    ),
    'js' => array(
      'plugin' => 'context_asset_manager_addjs',
    ),

    // Module.
    'css_module' => array(
      'plugin' => 'context_asset_manager_addcss_module',
    ),
    'js_module' => array(
      'plugin' => 'context_asset_manager_addjs_module',
    ),

    // File path.
    'css_path' => array(
      'plugin' => 'context_asset_manager_addcss_path',
    ),
    'js_path' => array(
      'plugin' => 'context_asset_manager_addjs_path',
    ),
  );

  if (module_exists('less')) {
    $registry['reactions']['less'] = array(
      'plugin' => 'context_asset_manager_addless',
    );
  }

  return $registry;
}

/**
 * Implements hook_context_page_reaction().
 *
 * @todo Generalize this so it's more pluggable
 */
function context_asset_manager_context_page_reaction() {
  // Theme.
  if ($plugin = context_get_plugin('reaction', 'css')) {
    $plugin->execute();
  }
  if (module_exists('less')) {
    if ($plugin = context_get_plugin('reaction', 'less')) {
      $plugin->execute();
    }
  }
  if ($plugin = context_get_plugin('reaction', 'js')) {
    $plugin->execute();
  }

  // Module.
  if ($plugin = context_get_plugin('reaction', 'css_module')) {
    $plugin->execute();
  }
  if ($plugin = context_get_plugin('reaction', 'js_module')) {
    $plugin->execute();
  }

  // File path.
  if ($plugin = context_get_plugin('reaction', 'css_path')) {
    $plugin->execute();
  }
  if ($plugin = context_get_plugin('reaction', 'js_path')) {
    $plugin->execute();
  }
}

/**
 * Scan drupal location for a type of file.
 *
 * @param string $filetype
 *   Extension of the file your looking for.
 *   Note: No leading period, for filetype use "js" instead of  ".js".
 * @param string $where
 *   The location function will search.
 *   Currently Supports: Modules, Themes (Active), and Abitrary File Paths.
 *
 * @return array
 *   An array indexed by file paths containing strings
 *   describing each path "Theme Key - File Name"
 */
function _context_asset_manager_scandir($filetype = NULL, $where = 'themes') {
  $file_files = FALSE;

  // If $filetype has leading "." - remove it.
  if ($filetype[0] == '.') {
    $filetype = drupal_substr(1, drupal_strlen($filetype));
  }

  // Future setting to include all themes.
  $include_all = FALSE;

  $mask = "/.+\." . $filetype . '/';
  switch ($where) {

    // If want to find assets in modules.
    case 'modules':

      // Grab selected modules.
      $modules = variable_get('context_asset_manager_scan_contrib_modules', array());
      if (variable_get('context_asset_manager_include_core_assets') == 1) {
        // Print 'pralay123';.
        $core_modules = variable_get('context_asset_manager_scan_core_modules', array());
        $modules = array_merge($modules, $core_modules);
      }

      foreach ($modules as $path) {
        if (!$path) {
          continue;
        }
        $files_raw[$path] = file_scan_directory($path, $mask);
      }
      break;

    // If want to find assets in file paths.
    case 'paths':
      // Initialize path array index at zero.
      $path_index = 0;

      // Grab every set file path.
      while ($path = variable_get('context_asset_manager_index_path' . $path_index, NULL)) {
        $files_raw[$path] = file_scan_directory($path, $mask);
        $path_index += 1;
      }
      break;

    // If you want to find assets in themes.
    case 'themes':
    default:
      // Setup vars to avoid foreach fails.
      $selected_themes = array();
      $files_raw = array();

      // We'll grab active themes.
      $themes = list_themes();

      foreach ($themes as $item) {

        // Only lists the theme if the theme is enabled.
        // Drupal's list_themes() function returns an array of objects,
        // so we extract an array from each of the objects.
        $list = get_object_vars($item);

        // Only list the theme if it is enabled.
        if ($include_all == FALSE and $list['status'] or $include_all == TRUE) {
          $list = get_object_vars($item);
          $path = explode('/', $list['filename']);

          if (variable_get('context_asset_manager_include_core_assets') == 0 && strpos($path, 'sites/') !== FALSE) {
            continue;
          }

          unset($path[count($path) - 1]);
          $path = implode('/', $path);
          $selected_themes[] = array(
            'name' => $list['info']['name'],
            'path' => $path,
          );
        }
      }
      // Scan $selected_themes for given filetype.
      foreach ($selected_themes as $theme) {
        $dir = $theme['path'];
        $name = $theme['name'];
        $mask = "/.+\." . $filetype . "/";
        $files_raw[$name] = file_scan_directory($dir, $mask);
      }
      break;

  } // switch $where

  // No files found.
  if (isset($files_raw)) {
    if (is_array($files_raw)) {
      // Remove the full path.
      foreach ($files_raw as $key => $value) {
        foreach ($value as $value_key => $file) {
          $file_files[$file->uri] = $key . ' -- ' . $file->uri;
        }
      }
    }
  }

  return $file_files;
}
